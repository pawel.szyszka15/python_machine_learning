import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from numpy import linalg as LA

A = np.array([[2.5, 0.5, 2.2, 1.9, 3.1, 2.3, 2, 1, 1.5, 1.1],
[2.4, 0.7, 2.9, 2.2, 3, 2.7, 1.6, 1.1, 1.6, 0.9]])
A = A.transpose()

plt.figure(figsize = (10,10))
plt.scatter(A[:, 0], A[:, 1])
plt.title('Data plot')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

# definition of fucntion to center data
center_function = lambda x: x - x.mean()

# apply function to axis
data_centered_x = center_function(A[:, 0])
data_centered_y = center_function(A[:, 1])

plt.figure(figsize = (10,10))
plt.scatter(data_centered_x, data_centered_y)
plt.axvline(x=0, color="grey")
plt.axhline(y=0, color="grey")
plt.title('Data plot')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

# Calculation of covariance matrix
A_cov = np.cov(data_centered_x, data_centered_y, rowvar=False)

# calculation of eigendecompostion
eigen_values, eigen_vectors = LA.eig(A_cov)

# plot with eigenvectors
data_centered_eigen_vectors = center_function(eigen_vectors)

plt.figure(figsize = (10,10))
plt.quiver(data_centered_eigen_vectors[0, 0], data_centered_eigen_vectors[1, 0], color=['r'], scale=5)
plt.quiver(data_centered_eigen_vectors[0, 1], data_centered_eigen_vectors[1, 1], color=['b'], scale=5)

plt.scatter(data_centered_x, data_centered_y)
plt.axvline(x=0, color="grey")
plt.axhline(y=0, color="grey")
plt.title('Data plot with eigenvectors')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

u = data_centered_eigen_vectors[:, 1]
z = data_centered_eigen_vectors[:, 0]

u = np.expand_dims(u, 1)
z = np.expand_dims(z, 1)

### projection for first eigenvector
# multiplication
# Y = X@u@uT
# @ - multiplication sign
Y = A @ u @ u.transpose()

# plot results
plt.figure(figsize = (10,10))
plt.quiver(data_centered_eigen_vectors[0, 0], data_centered_eigen_vectors[1, 0], color=['r'], scale=5)
plt.quiver(data_centered_eigen_vectors[0, 1], data_centered_eigen_vectors[1, 1], color=['b'], scale=5)

data_projecred_centered_x = center_function(Y[:, 0])
data_projected_centered_y = center_function(Y[:, 1])

plt.scatter(data_projecred_centered_x, data_projected_centered_y)
plt.axvline(x=0, color="grey")
plt.axhline(y=0, color="grey")
plt.title('Projected data plot with eigenvectors')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

# projection for second eigenvector
Y2 = A @ z @ z.transpose()

# plot results
plt.figure(figsize = (10,10))
plt.quiver(data_centered_eigen_vectors[0, 0], data_centered_eigen_vectors[1, 0], color=['r'], scale=5)
plt.quiver(data_centered_eigen_vectors[0, 1], data_centered_eigen_vectors[1, 1], color=['b'], scale=5)

data_projecred_centered_x2 = center_function(Y2[:, 0])
data_projected_centered_y2 = center_function(Y2[:, 1])

plt.scatter(data_projecred_centered_x2, data_projected_centered_y2)
plt.axvline(x=0, color="grey")
plt.axhline(y=0, color="grey")
plt.title('Projected data plot with eigenvectors for second eigenvector')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

print("Calculated values of the parameters")
print("Covariance matrix =")
print(A_cov)
