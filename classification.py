import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.model_selection import train_test_split

col_names = ['data_1', 'data_2', 'label']
my_data = pd.read_csv('~/Documents/ml/Lab2/data.csv', header=None, names=col_names).to_numpy()

colors = ['red','green','blue','purple']
plt.scatter(my_data[:, 0], my_data[:, 1], c=my_data[:, 2], cmap=matplotlib.colors.ListedColormap(colors))
plt.title('Data plot')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

clf = DecisionTreeClassifier(max_depth=9, min_samples_leaf=1, random_state=0) # defualt hyperparameters: max_depth=None, min_samples_leaf=1,
X = my_data[:, :1]
y = my_data[:, 2]

# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1) # 70% training and 30% test

clf = clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
accuracy = clf.score(X, y)

### Present results
plt.figure()
plot_tree(clf, filled=True)
plt.title("Decision tree")
plt.show()

plot_colors = "br"
plot_step = 0.02
class_names = "AB"

plt.figure(figsize=(10, 5))

# Plot the two-class decision scores
clf2 = AdaBoostClassifier(DecisionTreeClassifier(max_depth=9, min_samples_leaf=1, random_state=0), algorithm="SAMME", n_estimators=200) 
clf2 = clf2.fit(X_train, y_train)
y_pred = clf2.predict(X_test)
twoclass_output = clf2.decision_function(X)
plot_range = (twoclass_output.min(), twoclass_output.max())
plt.subplot(122)
for i, n, c in zip(range(2), class_names, plot_colors):
    plt.hist(
        twoclass_output[y == i],
        bins=10,
        range=plot_range,
        facecolor=c,
        label="Class %s" % n,
        alpha=0.5,
        edgecolor="k",
    )
x1, x2, y1, y2 = plt.axis()
plt.axis((x1, x2, y1, y2 * 1.2))
plt.legend(loc="upper right")
plt.ylabel("Samples")
plt.xlabel("Score")
plt.title("Decision Scores")

plt.tight_layout()
plt.subplots_adjust(wspace=0.35)
plt.show()

# Plot boundaries
xx, yy = np.meshgrid(X_train, y_train)
y_mesh_predict = clf.predict(np.c_[xx.ravel(), yy.ravel()]).reshape(xx.shape)
plt.imshow(y_mesh_predict, extent=(0, 1, 0, 1),
           cmap = "Wiistia", origin = "lower")
plt.scatter(my_data[:, 0], my_data[:, 1],
            c = y_train, cmap = "viridis")
plt.xlabel("X")
plt.xlabel("Y")
plt.title("Data plot with decision boundary")
plt.show()
