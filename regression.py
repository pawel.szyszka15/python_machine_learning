import numpy as np
import matplotlib.pyplot as plt

X = 0.4 * np.linspace(-3, 3, 500).reshape(500, 1)
y = 6 + 4 * X + np.random.randn(500, 1)

# Least Squares method
from sklearn.linear_model import LinearRegression, SGDRegressor
lin=LinearRegression().fit(X, y)
print("Least Square method:")
print(f"Equation: y = a*x + b ")
print(f"intercept (b) : {lin.intercept_}")
print(f"slope (a) : {lin.coef_}")
print(f"Equation: y = {lin.coef_}*x + {lin.intercept_}")

plt.scatter(X, y)
plt.plot(X, lin.predict(X), color = 'red')
plt.title('Linear Regression')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

# SGD method
from sklearn.pipeline import make_pipeline  # pipeline is used to scale the input data
from sklearn.preprocessing import StandardScaler
#rng = np.random.RandomState(0)
reg = make_pipeline(StandardScaler(), SGDRegressor(max_iter=1000, tol=1e-3))

reg = SGDRegressor(max_iter=100).fit(X, y)
sgd_parameters = reg.score(X, y)  # obtain linear equation's parameters

print("SGD (Stochastic Gradient Descent) method:")
print(f"Equation: y = a*x + b ")
print(f"intercept (b) : {reg.intercept_}")
print(f"slope (a) : {reg.coef_}")
print(f"Equation: y = {reg.coef_}*x + {reg.intercept_}")

plt.scatter(X, y)
plt.plot(X, reg.predict(X), color = 'red')
plt.title('Linear Regression')
plt.xlabel('x')
plt.ylabel('y')
plt.show()
